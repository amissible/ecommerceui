import { Component,OnInit, ViewChild } from '@angular/core';
import { HttpproductsService } from 'src/app/service/httpproducts.service';

declare var $: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit{ 

  @ViewChild('dataTable', { static: true }) table;
  dataTable: any;
  dtOptions: any = {};
  constructor() { }

  ngOnInit(): void {
    this.dtOptions = { 'pagingType' : 'full_numbers' };    
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOptions);
  }

}
