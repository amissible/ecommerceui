import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export class ProductList{
  category: any;
  product_name: any;
  alias : any;
  icon1_path : any;
  icon2_path : any;
}
export class ProductDetail{
  category: any;
  product_name: any;
  alias : any;
  image_path : any; //0-5
  info : any;  //0-2
}
export class QuickProduct{
  product_name: any;
  alias : any;
  image_path: any; //0-5
  info : any; //0-1
}

@Injectable({
  providedIn: 'root'
})
export class HttpproductsService {

  constructor(private httpproduct: HttpClient) { }

  getProducts() {
    return this.httpproduct.get<ProductList>('http://www.amissible.com:8082/db/catalog');
  }

  getProductDetails(alias) {
    return this.httpproduct.get<ProductDetail>('http://www.amissible.com:8082/db/product/' + alias);
  }
  getQuickProduct(alias) {
    return this.httpproduct.get<QuickProduct>('http://www.amissible.com:8082/db/quickview/' + alias);
  }
}
