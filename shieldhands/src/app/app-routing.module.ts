import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './product/product/product.component'
import { QuickviewComponent } from "./product/quickview/quickview.component";
import { HomeComponent } from './home/home/home.component';
import { ShopComponent } from './shop/shop/shop.component';
import { ContactUsComponent } from './contact/contact-us/contact-us.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'shop', component: ShopComponent },
  { path: 'contactUs', component: ContactUsComponent },
  { path: 'productDetails/:alias', component: ProductComponent },
  { path: 'productDetails', component: ShopComponent },
  { path: 'quickView/:alias', component: QuickviewComponent },
  { path: 'quickView', component: ShopComponent },
  { path: '',  redirectTo: '/home', pathMatch: 'full' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
