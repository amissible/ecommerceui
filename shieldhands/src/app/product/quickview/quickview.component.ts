import { Component, OnInit } from '@angular/core';
import { HttpproductsService } from '../../service/httpproducts.service';
import {Router, ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-quickview',
  templateUrl: './quickview.component.html',
  styleUrls: ['./quickview.component.css']
})
export class QuickviewComponent implements OnInit {

  Productalias: any;
  productDetail: any;

imgpath = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Blue-Bracelet-Canada.png";
imgpath2 = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Grey-Bracelet-Canada.png";
imgpath3 = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Peach-Bracelet-Canada.png";
imgpath4 = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Yellow-Bracelet-Canada.png";


  constructor(
    private httpproduct: HttpproductsService,
    private router: Router,
    private actRoute: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.Productalias = this.actRoute.snapshot.params['alias'];
    this.loadProductDetails(this.Productalias);
  }

  loadProductDetails(Productalias){
    this.httpproduct.getQuickProduct(Productalias).subscribe(product => {
      this.productDetail = product;
    });
  }

  navigation(link){
    this.router.navigate([link]);
  }

}
