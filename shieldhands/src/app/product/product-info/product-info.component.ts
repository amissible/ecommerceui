import { Component, OnInit } from '@angular/core';
import { HttpproductsService } from '../../service/httpproducts.service';
import {Router, ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {

  Productalias: any;
  productDetail: any;

  constructor(
    private httpproduct: HttpproductsService,
    private router: Router,
    private actRoute: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.Productalias = this.actRoute.snapshot.params['alias'];
    this.loadProductDetails(this.Productalias);
  }

  loadProductDetails(Productalias){
    this.httpproduct.getProductDetails(Productalias).subscribe(product => {
      this.productDetail = product;
    });
  }

  navigation(link){
    this.router.navigate([link]);
  }
  
}
