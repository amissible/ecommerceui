import { Component, OnInit } from '@angular/core';
import { HttpproductsService, ProductList } from '../../service/httpproducts.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products : ProductList;
  totalRecords: Number;
  page : Number = 1;
  pageSize =10;

  constructor(private httpproduct: HttpproductsService) { }

  ngOnInit(): void {
    this.httpproduct.getProducts().subscribe(
      response =>{
        this.products = response;
        //this.totalRecords = response.length();
      }
     );
  }

}
