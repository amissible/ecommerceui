import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiliconeComponent } from './silicone.component';

describe('SiliconeComponent', () => {
  let component: SiliconeComponent;
  let fixture: ComponentFixture<SiliconeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SiliconeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiliconeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
