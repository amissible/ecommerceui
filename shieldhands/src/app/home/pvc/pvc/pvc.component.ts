import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pvc',
  templateUrl: './pvc.component.html',
  styleUrls: ['./pvc.component.css']
})
export class PvcComponent implements OnInit {

  imgpath1 = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Blue-Bracelet-Canada.png";
  imgpath2 = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Grey-Bracelet-Canada.png";
  imgpath3 = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Peach-Bracelet-Canada.png";
  imgpath4 = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Yellow-Bracelet-Canada.png";
  imgpath5 = "https://shieldhands.com/wp-content/uploads/2020/11/Blue-side-1024x1024.jpg";
  imgpath6 = "https://shieldhands.com/wp-content/uploads/2020/10/Grey-side-1024x1024.jpg";
  imgpath7 = "https://shieldhands.com/wp-content/uploads/2020/10/pink-side-view-1024x1024.jpg";
  imgpath8 = "https://shieldhands.com/wp-content/uploads/2020/10/white-side.jpg";

  constructor() { }

  ngOnInit(): void {
  }

}
