import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ProductsComponent } from './shop/products/products.component';
import { FilterPipe } from './pipservice/filter.pipe';
import { CategoryComponent } from './shop/category/category.component';
import { ShopComponent } from './shop/shop/shop.component';
import { SortingPipe } from './pipservice/sorting.pipe';
import { ProductDetailsComponent } from './product/product-details/product-details.component';
import { ProductInfoComponent } from './product/product-info/product-info.component';
import { RelatedProductComponent } from './product/related-product/related-product.component';
import { ProductImageComponent } from './product/product-image/product-image.component';
import { ProductComponent } from './product/product/product.component';
import { QuickviewComponent } from './product/quickview/quickview.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './navigation/navbar/navbar.component';
import { SearchComponent } from './navigation/search/search.component';
import { TopNavComponent } from './navigation/top-nav/top-nav.component';
import { NavComponent } from './navigation/nav/nav.component';
import { SignInComponent } from './navigation/sign-in/sign-in.component';
import { CartComponent } from './navigation/cart/cart.component';
import { FooterComponent } from './navigation/footer/footer.component';
import { HomeComponent } from './home/home/home.component';
import { SliderComponent } from './home/slider/slider.component';
import { ProductSlideComponent } from './home/product-slide/product-slide.component';
import { SiliconeComponent } from './home/silicone/silicone/silicone.component';
import { PvcComponent } from './home/pvc/pvc/pvc.component';
import { SideNavComponent } from './navigation/side-nav/side-nav.component';

@NgModule({
  declarations: [
    AppComponent,
    ShopComponent,
    CategoryComponent,
    ProductsComponent,
    FilterPipe,
    SortingPipe,
    ProductDetailsComponent,
    ProductInfoComponent,
    RelatedProductComponent,
    ProductImageComponent,
    ProductComponent,
    QuickviewComponent,
    NavbarComponent,
    SearchComponent,
    TopNavComponent,
    NavComponent,
    SignInComponent,
    CartComponent,
    FooterComponent,
    HomeComponent,
    SliderComponent,
    ProductSlideComponent,
    SiliconeComponent,
    PvcComponent,
    SideNavComponent



  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FlexModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
