import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateByAliasComponent } from './update-by-alias.component';

describe('UpdateByAliasComponent', () => {
  let component: UpdateByAliasComponent;
  let fixture: ComponentFixture<UpdateByAliasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateByAliasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateByAliasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
