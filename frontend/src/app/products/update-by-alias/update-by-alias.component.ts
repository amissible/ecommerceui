import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpserviceService } from "../../service/httpservice.service";

@Component({
  selector: 'app-update-by-alias',
  templateUrl: './update-by-alias.component.html',
  styleUrls: ['./update-by-alias.component.css']
})
export class UpdateByAliasComponent implements OnInit {

  currentProduct = null;
  message = '';

  constructor(
    private httpService: HttpserviceService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.getProduct(this.route.snapshot.paramMap.get('alias'));
  }

  getProduct(alias): void {
    this.httpService.view(alias)
      .subscribe(
        data => {
          this.currentProduct = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  updatePublished(status): void {
    const data = {
      product_name: this.currentProduct.product_name,
      alias: this.currentProduct.alias,
      brand: this.currentProduct.brand,
      category: this.currentProduct.category,
      url: this.currentProduct.url,
      icon1_path: this.currentProduct.icon1_path,
      icon2_path: this.currentProduct.icon2_path,
      image_path: this.currentProduct.image_path,
      product_status: status,
    };

    this.httpService.update(this.currentProduct.alias, data)
      .subscribe(
        response => {
          this.currentProduct.product_status = status;
          console.log(response);
        },
        error => {
          console.log(error);
        });
  }

  updateProduct(): void {
    this.httpService.update(this.currentProduct.alias, this.currentProduct)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'The product was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  deleteProduct(): void {
    this.httpService.delete(this.currentProduct.alias)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/view']);
        },
        error => {
          console.log(error);
        });
  }

}
