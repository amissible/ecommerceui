import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { HttpserviceService } from "../../service/httpservice.service";

@Component({
  selector: 'app-add-products',
  templateUrl: './add-products.component.html',
  styleUrls: ['./add-products.component.css']
})
export class AddProductsComponent implements OnInit {

  products = {
    product_name: '',
    alias: '',
    brand: '',
    category: '',
    url: '',
    icon1_path: '',
    icon2_path: '',
    image_path: '',
    product_status: 'Available',
  };
  submitted = false;

  constructor(private httpservice: HttpserviceService) { }

  ngOnInit(): void {
  }

  saveProducts(): void {
    const data = {
      product_name: this.products.product_name,
      alias: this.products.alias,
      brand: this.products.brand,
      category: this.products.category,
      url: this.products.url,
      icon1_path: this.products.icon1_path,
      icon2_path: this.products.icon2_path,
      image_path: this.products.image_path,
    };

    this.httpservice.add(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newProducts(): void {
    this.submitted = false;
    this.products = {
      product_name: '',
      alias: '',
      brand: '',
      category: '',
      url: '',
      icon1_path: '',
      icon2_path: '',
      image_path: '',
      product_status: 'Available',
    };
  }

}
