import { Component, OnInit,ViewChild } from '@angular/core';
import { HttpserviceService } from "../../service/httpservice.service";

declare var $:any;

@Component({
  selector: 'app-view-products',
  templateUrl: './view-products.component.html',
  styleUrls: ['./view-products.component.css']
})
export class ViewProductsComponent implements OnInit {

 /* @ViewChild('dataTable', { static: true }) table;
  dataTable: any;
  dtOptions: any = {};*/

  products: any;
  currentProduct = null;
  currentIndex = -1;
  alias = '';
  constructor(private httpservice: HttpserviceService) { }

  ngOnInit(): void {
    /*this.dtOptions = { 'pagingType' : 'full_numbers' };    
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOptions);*/
    this.retrieveProducts();
  }

  retrieveProducts(): void {
    this.httpservice.viewall()
      .subscribe(
        data => {
          this.products = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveProducts();
    this.currentProduct = null;
    this.currentIndex = -1;
  }

  setActiveProduct(product, index): void {
    this.currentProduct = product;
    this.currentIndex = index;
  }

  removeAllProducts(): void {
    this.httpservice.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.retrieveProducts();
        },
        error => {
          console.log(error);
        });
  }

}
