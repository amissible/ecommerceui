import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteByAliasComponent } from './delete-by-alias.component';

describe('DeleteByAliasComponent', () => {
  let component: DeleteByAliasComponent;
  let fixture: ComponentFixture<DeleteByAliasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteByAliasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteByAliasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
