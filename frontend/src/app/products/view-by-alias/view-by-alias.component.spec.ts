import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewByAliasComponent } from './view-by-alias.component';

describe('ViewByAliasComponent', () => {
  let component: ViewByAliasComponent;
  let fixture: ComponentFixture<ViewByAliasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewByAliasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewByAliasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
