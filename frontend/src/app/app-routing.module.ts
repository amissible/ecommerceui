import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddProductsComponent } from "./products/add-products/add-products.component";
import { DeleteByAliasComponent } from "./products/delete-by-alias/delete-by-alias.component";
import { DeleteProductsComponent } from "./products/delete-products/delete-products.component";
import { UpdateByAliasComponent } from "./products/update-by-alias/update-by-alias.component";
import { UpdateProductsComponent } from "./products/update-products/update-products.component";
import { ViewByAliasComponent } from "./products/view-by-alias/view-by-alias.component";
import { ViewProductsComponent } from "./products/view-products/view-products.component";

const routes: Routes = [
  { path: 'add', component: AddProductsComponent },
  { path: 'update/:alias', component: UpdateByAliasComponent },
  { path: 'update', component: UpdateProductsComponent },
  { path: 'delete/:alias', component: DeleteByAliasComponent },
  { path: 'delete', component: DeleteProductsComponent },
  { path: 'view/:alias', component: ViewByAliasComponent },
  { path: 'view', component: ViewProductsComponent },
  { path: '',  redirectTo: '/view', pathMatch: 'full' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
