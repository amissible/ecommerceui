import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpserviceService {

  constructor(private http: HttpClient) { }

  viewall():Observable<any>{
   return this.http.post("localhost:8089/product/view");
    // return this.http.get("http://www.amissible.com:8081/db/catalog");
  }

  view(alias):Observable<any>{
    return this.http.get("");
  }
  
  deleteAll():Observable<any>{
    return this.http.delete("");
  }
  
  delete(alias):Observable<any>{
    return this.http.delete("");
  }
  
  update(alias, data):Observable<any>{
    return this.http.put("",data);
  }
  
  add(data):Observable<any>{
    return this.http.post("",data);
  }
}
