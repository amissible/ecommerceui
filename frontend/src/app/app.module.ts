import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MatStepperModule } from '@angular/material/stepper';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MatToolbarModule } from "@angular/material/toolbar"; 
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatProgressBarModule } from "@angular/material/progress-bar";

import { AppRoutingModule } from './app-routing.module';
import { AddProductsComponent } from "./products/add-products/add-products.component";
import { ViewProductsComponent } from './products/view-products/view-products.component';
import { UpdateProductsComponent } from './products/update-products/update-products.component';
import { UpdateByAliasComponent } from './products/update-by-alias/update-by-alias.component';
import { DeleteByAliasComponent } from './products/delete-by-alias/delete-by-alias.component';
import { DeleteProductsComponent } from './products/delete-products/delete-products.component';
import { ViewByAliasComponent } from './products/view-by-alias/view-by-alias.component';

@NgModule({
  declarations: [
    AppComponent,
    AddProductsComponent,
    ViewProductsComponent,
    UpdateProductsComponent,
    UpdateByAliasComponent,
    DeleteByAliasComponent,
    DeleteProductsComponent,
    ViewByAliasComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    MatStepperModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,  
    MatButtonModule,  
    MatCardModule,  
    MatProgressBarModule,
  ],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
