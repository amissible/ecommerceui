package com.amissible.payment.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amissible.payment.exception.AppException;
import com.amissible.payment.model.Config_payment_manager;
import com.paypal.base.rest.OAuthTokenCredential;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;

@Service
public class PaypalService {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public Payment createPayment(
			Double total, 
			String currency, 
			String method,
			String intent,
			String alias,
			String description,
			String cancelUrl, 
			String successUrl) throws PayPalRESTException{
		Amount amount = new Amount();
		amount.setCurrency(currency);
		total = new BigDecimal(total).setScale(2, RoundingMode.HALF_UP).doubleValue();
		amount.setTotal(String.format("%.2f", total));

		Transaction transaction = new Transaction();
		transaction.setDescription(alias + "-" + description);
		transaction.setAmount(amount);

		List<Transaction> transactions = new ArrayList<>();
		transactions.add(transaction);

		Payer payer = new Payer();
		payer.setPaymentMethod(method.toString());

		Payment payment = new Payment();
		payment.setIntent(intent.toString());
		payment.setPayer(payer);  
		payment.setTransactions(transactions);
		RedirectUrls redirectUrls = new RedirectUrls();
		redirectUrls.setCancelUrl(cancelUrl);
		redirectUrls.setReturnUrl(successUrl);
		payment.setRedirectUrls(redirectUrls);

		return payment.create(getApiContext(alias));
	}
	
	public Payment executePayment(String alias,String paymentId, String payerId) throws PayPalRESTException{
		Payment payment = new Payment();
		payment.setId(paymentId);
		PaymentExecution paymentExecute = new PaymentExecution();
		paymentExecute.setPayerId(payerId);
		return payment.execute(getApiContext(alias), paymentExecute);
	}

	public APIContext getApiContext(String org_alias) throws PayPalRESTException{
		Config_payment_manager config = getConfigFromDB(org_alias);
		Map<String, String> configMap = new HashMap<>();
		configMap.put("mode","live");
		OAuthTokenCredential auth = new OAuthTokenCredential(config.getPaypal_client_id(),config.getPaypal_client_secret(),configMap);
		APIContext context = new APIContext(auth.getAccessToken());
		context.setConfigurationMap(configMap);
		return context;
	}

	public Config_payment_manager getConfigFromDB(String org_alias){
		List<Config_payment_manager> list = new ArrayList<>();
		jdbcTemplate.query(
				"SELECT * FROM config_payment_manager WHERE org_alias = ?", new Object[] { org_alias },
				(rs, rowNum) -> new Config_payment_manager(
						rs.getLong("id"),
						rs.getString("org_name"),
						rs.getString("org_alias"),
						rs.getString("validity_from"),
						rs.getString("validity_until"),
						rs.getString("url_success"),
						rs.getString("url_cancel"),
						rs.getString("paypal_client_id"),
						rs.getString("paypal_client_secret"),
						rs.getString("register_timestamp")
				)
		).forEach(record -> list.add(record));
		if(list.size() == 0)
			throw new AppException("Organization is not authorised", HttpStatus.UNAUTHORIZED);
		if(list.size() > 1)
			throw new AppException("Duplicate configuration found", HttpStatus.CONFLICT);
		return list.get(0);
	}
}
