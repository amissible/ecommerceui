export const environment = {
  production: true,
  catalogRestHost : "http://www.amissible.com:8082/",
  productDetailEndPoint : "db/product/",
  productQVEndPoint : "static/quickview"
};
