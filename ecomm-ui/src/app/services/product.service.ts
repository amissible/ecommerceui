import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

export interface Product {
    category : any;
    product_name: any;
    alias : any;
    info : any;
    image_path : any;
    attributes : any;
}

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  restProdDetailsEndPoint : string;
  restProdQVEndPoint : string;
    

  constructor( private httpproduct: HttpClient ) {
          this.restProdDetailsEndPoint = environment.catalogRestHost+environment.productDetailEndPoint;
          this.restProdQVEndPoint = environment.catalogRestHost+environment.productQVEndPoint;
          console.log(this.restProdDetailsEndPoint);
       }

  /**
   * Gets product details
   * @param productName 
   */ 
  public getProduct(productName:string) {
    return this.httpproduct.get<Product>(this.restProdDetailsEndPoint+productName);
  }

  /**
   * Gets product Quick View
   * @param productName 
   */
  public getQuickProduct(productName:string) {
    return this.httpproduct.get<Product>(this.restProdQVEndPoint+productName);
  }
}
