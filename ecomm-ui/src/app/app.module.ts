import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import HomeComponent from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ShopComponent } from './shop/shop.component';
import { ProductComponent } from './product/product.component';
import { ShoppingListComponent } from './shop/shopping-list/shopping-list.component';
import { ShoppingCartComponent } from './shop/shopping-cart/shopping-cart.component';
import { QuickViewComponent } from './product/quick-view/quick-view.component';
import { DetailViewComponent } from './product/detail-view/detail-view.component';
import { BannerSliderComponent } from './home/banner-slider/banner-slider.component';
import { HowToComponent } from './home/how-to/how-to.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ContactUsComponent,
    ShopComponent,
    ProductComponent,
    ShoppingListComponent,
    ShoppingCartComponent,
    QuickViewComponent,
    DetailViewComponent,
    BannerSliderComponent,
    HowToComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
