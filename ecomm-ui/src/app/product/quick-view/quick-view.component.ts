import { Component, OnInit } from '@angular/core';
import {Product, ProductService} from '../../services/product.service';

@Component({
  selector: 'app-quick-view',
  templateUrl: './quick-view.component.html',
  styleUrls: ['./quick-view.component.css']
})
export class QuickViewComponent implements OnInit {
  product : Product;

  constructor(private productServices : ProductService) { 
     this.product = {} as Product;
     
  }

  ngOnInit(): void {
    this.productServices.getQuickProduct("blue-sanitizer-bracelet").subscribe(
      response =>{
        console.log("Getting blue sanatizer product")
        this.product = response;
      }
     );
    
  }

}
