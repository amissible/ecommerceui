import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import HomeComponent from './home/home.component'
import { ShopComponent } from './shop/shop.component'
import { ContactUsComponent } from './contact-us/contact-us.component'

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'shop', component: ShopComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: '', redirectTo : '/home', pathMatch:'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
